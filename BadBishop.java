public class B extends P implements Serializable{

	final private String n="Bishop";

	public B(C c, Coo coo) {
		super(c, coo);
	}
	
	@Override
	public boolean isM(Coo toCo){
		if(Math.abs(toCo.X() - X) == Math.abs(toCo.Y() - Y)){
            return true;
		}
		return false;
	}

	@Override
	public boolean isA(Coo toC) {
		return isM(toC);
	}

	public String gM(){
		return n;
	}
}
