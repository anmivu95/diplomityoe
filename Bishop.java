
public class Bishop extends Piece implements Serializable{

	final private String name="Bishop";

	public Bishop(Colour colour, CartesianCoordinate coordinate) {
		super(colour, coordinate);
	}
	
	@Override
	public boolean isMovePossible(CartesianCoordinate toCoordinate){
		if(Math.abs(toCoordinate.getX() - fromX) == Math.abs(toCoordinate.getY() - fromY)){
            return true;
		}
		return false;
	}

	@Override
	public boolean isAttackPossible(CartesianCoordinate toCoordinate) {
		return isMovePossible(toCoordinate);
	}

	public String getName(){
		return name;
	}
}
